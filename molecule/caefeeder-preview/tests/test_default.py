import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/caefeeder-preview",
    "/opt/coremedia/caefeeder-preview/bin",
    "/opt/coremedia/caefeeder-preview/corem_home"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/caefeeder-preview.fact",
    "/opt/coremedia/caefeeder-preview/caefeeder-preview.jar",
    "/opt/coremedia/caefeeder-preview/application.properties",
    "/opt/coremedia/caefeeder-preview/jmxremote.access",
    "/opt/coremedia/caefeeder-preview/jmxremote.password",
    "/opt/coremedia/caefeeder-preview/jaas.conf",
    "/opt/coremedia/caefeeder-preview/bin/post-start-check.sh",
    "/opt/coremedia/caefeeder-preview/bin/pre-start-check.sh",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.group("coremedia").exists
    assert host.user("coremedia").exists
    assert host.user("caefeeder-preview").exists
    assert host.user("caefeeder-preview").shell == "/sbin/nologin"
    assert host.user("caefeeder-preview").home == "/opt/coremedia/caefeeder-preview"


def test_properties(host):
    property_file = "/opt/coremedia/caefeeder-preview/application.properties"

    assert host.file(property_file).contains("repository.url")
    assert host.file(property_file).contains("jdbc.driver")
    assert host.file(property_file).contains("jdbc.url")
    assert host.file(property_file).contains("jdbc.user")
    assert host.file(property_file).contains("jdbc.password")
    assert host.file(property_file).contains("solr.url")


def test_service(host):
    service = host.service("caefeeder-preview")
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize("ports", [
    '0.0.0.0:40799',
    '127.0.0.1:40080',
])
def test_open_port(host, ports):

    for i in host.socket.get_listening_sockets():
        print( i )

    application = host.socket("tcp://{}".format(ports))
    assert application.is_listening
