# CoreMedia - generic ansible role for caefeeder

This role can deploy all (currently known) caefeeder types.

Like *caefeeder-live* and *caefeeder-preview*.

Since CMCC-10 spring-boot is the preferred way to roll out an application (after docker).

Here, we use the spring-boot way.

With spring-boot a few special features were integrated:

- In the defaults.yml an extra configuration parameter `spring_boot_overwrite` was integrated.
  <br>Thats overwrite the default parameters in `vars/main.yml`
- A systemd unit configuration is created for each service so that the unit file does not have to be adjusted.
- JMX monitoring can now be explicitly enabled or deactivated.


Furthermore, it is no longer necessary to extend the IOR path with the service name for *spring-boot*.

Therefore `http://mls.cm.local:40280/master-live-server/ior` becomes `http://mls.cm.local:40280/ior`

Besides **mysql**, **postgres** is now supported as database type.
Here you only have to specify the corresponding type at `database_driver`.

## Requirement

- java 11 (e.g. [corretto](https://github.com/bodsch/ansible-corretto-java))
- [coremedia](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/coremedia)

## Dependent Services

### caefeeder-live

- solr
```
application_properties:
  solr.url: http://solr.int:40080/solr
```

- database
```
application_database:
  host: frontend_database.int
  schema: caefeeder_live
  user: caefeeder-live
  password: caefeeder-live
```

### caefeeder-preview

- solr
```
application_properties:
  solr.url: http://solr.int:40080/solr
```

- database
```
content_server_database:
  host: backend_database.int
  schema: caefeeder_preview
  user: caefeeder-preview
  password: caefeeder-preview
```



## Configuration

To set a own *heap* memory use `heap_memory` (default are `512`). **Please use only Megabyte data!**

The default Out-Of-Memory Handler for *OnOutOfMemoryError* are `kill -9 %p`.
You can also use the [oom-notifier](https://gitlab.com/coremedia-as-code/deployment/ansible-roles/coremedia/-/blob/master/templates/oom-notifier.sh.j2) script of the User *coremedia* `/opt/coremedia/bin/oom-notifier.sh`.
For this purpose, only the `oom_notifier` variable must be set accordingly.

With `jmx_monitoring_enabled` you can enable / disable the JMX Monitoring Interface.

For more detailed information please look into the `default/main.yml` or in the `vars/main.yml`!

With `elastic_url_bulk` you can specify an elasticsearch server to establish a central logging.
As an example: `elastic_url_bulk: 'http://elastic.cm.local:9200/_bulk`


## Examples

- caefeeder-live

```
---
- hosts: caefeeder-live
  roles:
    - role: postgresql
    - role: corretto
    - role: solr
    - role: caefeeder
      vars:
        - caefeeder: caefeeder-live
        - coremedia:
            caefeeder_live:
              artefact_name: caefeeder.jar
```

- caefeeder-preview

```
- hosts: caefeeder-preview
  roles:
    - role: postgresql
    - role: corretto
    - role: solr
    - role: caefeeder
      vars:
        - caefeeder: caefeeder-preview
        - coremedia:
            caefeeder_preview:
              artefact_name: caefeeder.jar
```


## Tests

```
$ tox -e py36-ansible29 -- molecule test --all
$ tox -e py36-ansible29 -- molecule test -s caefeeder-live
$ tox -e py36-ansible29 -- molecule test -s caefeeder-preview
```

## Service relationships


![setup](caefeeder.png)
